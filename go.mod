module gitlab.com/xmarlem1/grpc/sample-grpc

go 1.19

require (
	github.com/cockroachdb/cmux v0.0.0-20170110192607-30d10be49292
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.15.0
	google.golang.org/grpc v1.52.3
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.2.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/golang/glog v1.0.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	google.golang.org/grpc/examples v0.0.0-20230201212035-3151e834fa25 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
