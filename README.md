# Sample-grpc


I though about:

- how to use go generate to generate my go code from protobuf and how to use generate feature in general in go
- the strcture.. having in mind as a reference what is done in grafeas project..
  - they use a proto folder where they store all proto files
  - in proto folder you find different subfolders, one for each version...e.g. v1, v1beta1, v1alpha1...
  - in each folder there is a generate.go
  - in each folder specific proto files
- I decided to use makefile just as an entry point, but to leverage mainly go generate feature



# go tools
In order to add grpc-gateway feature, I checked the readme in grpc-gateway repo and they suggested to use the tool dependency best practice, which is a gohper way to manage binary tools required when developing a go project.
E.g. if you need tools like linter or code generators... a good practice to make sure you and contributors use the same version of those tools... and also to have the same tools locally available...
NB> make sure to set GOBIN in your PATH and to set that to local $PWD/bin

If you do that, once you run:
$ go install \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
    google.golang.org/protobuf/cmd/protoc-gen-go \
    google.golang.org/grpc/cmd/protoc-gen-go-grpc

you get a new bin folder with all required binaries.
