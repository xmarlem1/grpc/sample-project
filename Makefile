.PHONY: gen clean git

M=.

# this is the old one... use generate instead, which is using go generate
gen:
	protoc \
	--proto_path=proto \
	--go_out=generated \
	--go_opt=paths=import \
	--go_opt=module=hello-grpc \
	proto/v1alpha1/*.proto


server:
	go run greeter_server/main.go

generate:
	go generate ./proto/v1alpha1
	go generate ./greeter_server

clean:
	@rm -rf proto/v1alpha1/go_proto/*.go
	@echo "Cleaned up"


git:
	git add .
	git commit -m "${M}"
	git push -u origin HEAD


# --go-grpc_out=. \
#	--go-grpc_opt=paths=source_relative \
