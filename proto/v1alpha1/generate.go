// =================
// Go proto generation
//

//go:generate rm -rf go_proto
//go:generate mkdir go_proto
//go:generate -command compile protoc --proto_path=../../googleapis --proto_path=../../proto/v1alpha1 --go_out=./go_proto --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go-grpc_out=require_unimplemented_servers=false:./go_proto --grpc-gateway_out ./go_proto --grpc-gateway_opt logtostderr=true --grpc-gateway_opt paths=source_relative --grpc-gateway_opt generate_unbound_methods=true

//go:generate echo "Building proto files"
//go:generate compile ../../proto/v1alpha1/hello_service.proto

package v1alpha1
