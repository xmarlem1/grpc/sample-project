package main

import (
  "context"
  "flag"
  "fmt"
  "log"
  "net"
  "net/http"

  "github.com/cockroachdb/cmux"
  "github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
  pb "gitlab.com/xmarlem1/grpc/sample-grpc/proto/v1alpha1/go_proto"
  "gitlab.com/xmarlem1/grpc/sample-grpc/service"

  "google.golang.org/grpc"
  "google.golang.org/grpc/reflection"
)

func main() {

  // flags
  port := flag.Int("port", 50053, "the server port")
  flag.Parse()

  // =============
  // grpc server
  // =============
  // creo un nuovo grpc server
  grpcServer := grpc.NewServer()
  // creo un nuovo greeter server
  greeterServer := service.NewGreeterServer()
  // registro il mio greeter server con il grpc server
  pb.RegisterGreeterServiceServer(grpcServer, greeterServer)

  // =============
  // gRPC gateway
  // =============
  // mux for gRPC gateway. This will multiplex or route request different gRPC service
  mux := runtime.NewServeMux()
  err := pb.RegisterGreeterServiceHandlerFromEndpoint(context.Background(), mux, fmt.Sprintf("localhost:%d", *port), []grpc.DialOption{grpc.WithInsecure()})
  if err != nil {
    panic(err)
  }

  // creating a normal http server
  server := http.Server{
    Handler: mux,
  }
  ////

  // creating a listener for server
  log.Printf("Start server on port %d", *port)
  address := fmt.Sprintf("0.0.0.0:%d", *port)
  listener, err := net.Listen("tcp", address)
  if err != nil {
    panic(err)
  }

  m := cmux.New(listener)

  httpL := m.Match(cmux.HTTP1Fast())

  grpcL := m.Match(cmux.HTTP2())

  // required for evans
  reflection.Register(grpcServer)

  // Start server
  go grpcServer.Serve(grpcL)
  go server.Serve(httpL)

  m.Serve()
}

func NewHelloRequest(name string) *pb.HelloRequest {
  return &pb.HelloRequest{
    Name: name,
  }
}
